"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.random = exports.fortunaInit = void 0;
const fortuna = require("javascript-fortuna");
const si = require("systeminformation");
const sha512 = require("js-sha512");
const async_mutex_1 = require("async-mutex");
const mutex = new async_mutex_1.Mutex();
let entropyVal;
let counter = 0;
const entropyAccumFunction = async () => {
    try {
        const processes = await si.processes();
        const memory = await si.mem();
        entropyVal = sha512(`${JSON.stringify(processes)}:${JSON.stringify(memory)}:${new Date().getTime()}`);
    }
    catch (err) {
        console.log(err);
    }
};
const entropyFxn = () => {
    if (counter >= 10000) {
        counter = 0;
        entropyAccumFunction();
    }
    return entropyVal;
};
const fortunaInit = async () => {
    if (process.env.NODE_ENV === "production") {
        try {
            let i = 0;
            while (i < 5) {
                console.log("Fortuna starting in:", 5 - i);
                await entropyAccumFunction();
                await new Promise((resolve) => setTimeout(resolve, 250));
                i++;
            }
            fortuna.init({
                entropyFxn,
            });
        }
        catch (err) {
            console.log(err);
        }
    }
    else {
        fortuna.init();
    }
};
exports.fortunaInit = fortunaInit;
const random = async () => {
    let random;
    await mutex
        .runExclusive(async () => {
        random = fortuna.random();
    })
        .catch((err) => {
        console.log(err);
    });
    return random;
};
exports.random = random;
//# sourceMappingURL=main.js.map